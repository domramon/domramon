/*
 * Prototype extension
 */

Element.prototype.css = function(a) {
    if(arguments.length <= 1) {
        for (let i in a) {
            this.style[i] = a[i];
        }
    } else {
        this.style[arguments[0]] = arguments[1];
    }
    return this;
};

Element.prototype.addClass = function(a) {
    for(let arg in  arguments) {
        this.classList.add(arguments[arg]);
        return this;
    }
};

Element.prototype.removeClass = function(a) {
    for(let arg in  arguments) {
        this.classList.remove(arguments[arg]);
        return this;
    }
};

Element.prototype.adopt = function(a){
    for (let arg in arguments) {
        this.append(arguments[arg]);
        return this;
    }
};

Element.prototype.attr = function () {
    if(arguments.length <= 1) {
        for (let i in a) {
            this.setAttribute(i, a[i]);
        }
    } else {
        this.setAttribute(arguments[0], arguments[1]);
    }
    return this;
};

Element.prototype.html = function(a) {
    this.innerHTML = a;
    return this;
};

Element.prototype.addEvents = function(ev) {
    this.addEventListener(arguments[0], arguments[1]);
    return this;
};
